//
//  ViewController.swift
//  MVVM+Json
//
//  Created by Faique Ali on 19/08/2020.
//  Copyright © 2020 Faique Ali. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    
    var viewModelUser = UserViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModelUser.vc = self   //no need to write tblView reload code
        //viewModelUser.getAllUserData() //URLSessiom
        viewModelUser.getAllUserDataUsingAlamofire() //Alamofire
        tblView.register(UINib(nibName: "UserCell", bundle: nil), forCellReuseIdentifier: "UserCell")
    }


}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModelUser.arrUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as? UserCell
        let modelUser = viewModelUser.arrUsers[indexPath.row]
        /*
         let status = modelUser.getStatusColor()
         cell?.lblStatus.text = status.0
         cell?.backgroundColor = status.1
         if let id = modelUser.id {
            cell?.lblID.text = "\(id)"
         } else {
            cell?.lblID.text = "No ID"
         }
         cell?.lblTitle.text = modelUser.title
         */
        cell?.modelUser = modelUser
        cell?.userConfiguration()
        return cell!
    }
    
    
}
