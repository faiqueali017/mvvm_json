//
//  UserViewModel.swift
//  MVVM+Json
//
//  Created by Faique Ali on 19/08/2020.
//  Copyright © 2020 Faique Ali. All rights reserved.
//

import UIKit
import Alamofire

class UserViewModel{
    
    weak var vc: ViewController?
    var arrUsers = [UserModel]()
    
    func getAllUserDataUsingAlamofire(){
        AF.request("https://jsonplaceholder.typicode.com/todos/").response { response in
            if let data = response.data {
                do {
                    let userResponse = try
                        JSONDecoder().decode([UserModel].self, from: data) //[UserModel] in arr
                    self.arrUsers.append(contentsOf: userResponse) //same for loop wala kaam
                    DispatchQueue.main.async {
                        self.vc?.tblView.reloadData()
                    }
                } catch let err{
                    print(err.localizedDescription)
                }
            }
            
        }
    }
    
    func getAllUserData(){
        URLSession.shared.dataTask(with: URL(string: "https://jsonplaceholder.typicode.com/todos/")!) { (data, response, error) in
            if error == nil {
                if let data = data {
                    do {
                        let userResponse = try
                            JSONDecoder().decode([UserModel].self, from: data) //[UserModel] in arr
                        //print(userResponse)
                        /*
                        for modelUser in userResponse{
                            self.arrUsers.append(modelUser)
                        }*/
                        self.arrUsers.append(contentsOf: userResponse) //same for loop wala kaam
                        //print(self.arrUsers)
                        DispatchQueue.main.async {
                            self.vc?.tblView.reloadData()
                        }
                    } catch let err{
                        print(err.localizedDescription)
                    }
                }
            } else {
                print(error?.localizedDescription)
            }
        }.resume()
    }
}
